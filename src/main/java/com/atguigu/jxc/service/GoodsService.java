package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    List<Goods> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    List<Goods> getGoodsListByNameType(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveOrUpdateGoods( Goods goods);

    ServiceVO deleteGoods(Integer goodsId);

    List<Goods> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);


    ServiceVO deleteStock(Integer goodsId);

    List<Goods> getAlarmList();
}
