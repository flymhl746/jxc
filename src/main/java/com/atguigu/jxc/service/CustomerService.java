package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {


    Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName);

    void saveOrUpdateCustomer( Customer customer);

    void deleteCustomer(String ids);
}
