package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DamageServiceImpl implements DamageService {
    @Autowired
    DamageListDao damageListDao;
    @Autowired
    DamageListGoodsDao damageListGoodsDao;
    @Autowired
    UserService userService;
    @Autowired
    UserDao userDao;
    @Override
    @Transactional
    public void saveDamageListGoods(DamageList damageList, String damageListGoodsStr,HttpSession session) {


        List<DamageListGoods>damageListGoodsList  = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);

        log.info("damageList:{}",damageList);
        log.info("damageList:{}",damageListGoodsList);
        Map<String, Object> map = userService.loadUserInfo(session);
        log.info("map:{}",map);
        String userName = (String) map.get("userName");
        Integer userId = userDao.getuserId(userName);
        damageList.setUserId(userId);
        damageListDao.saveDamageList(damageList);
        Integer damageListId = damageListDao.getDamageListId(damageList.getDamageNumber());
        damageListGoodsList = damageListGoodsList.stream().map(damageListGoods -> {
            damageListGoods.setDamageListId(damageListId);
            return damageListGoods;
        }).collect(Collectors.toList());
        damageListGoodsDao.saveDamageListGoods(damageListGoodsList);

    }

    @Override
    public List<DamageList> getDamageList(String sTime, String eTime, HttpSession session) {
        Map<String, Object> map = userService.loadUserInfo(session);
        log.info("map:{}",map);
        String userName = (String) map.get("userName");
        List<DamageList> damageLists= damageListDao.getDamageListByTime(sTime,eTime);
        damageLists =damageLists.stream().map(damageList -> {damageList.setTrueName(userName);
        return damageList;}).collect(Collectors.toList());
        return damageLists;
    }

    @Override
    public List<DamageListGoods> getDamageListGoodsList(Integer damageListId) {

        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getDamageListGoodsList(damageListId);
        return damageListGoodsList;
    }
}
