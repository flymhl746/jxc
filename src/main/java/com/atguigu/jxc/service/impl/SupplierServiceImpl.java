package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    SupplierDao supplierDao;

    @Override
    public void deleteSupplier(String ids) {
        String[] split = ids.split(",");
        List<String> idList = Arrays.asList(split);
        List<Integer> list = idList.stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());
        supplierDao.deleteSupplier(list);
    }

    @Override
    public void saveOrUpdateSupplier(Supplier supplier) {
        if(supplier.getSupplierId()==null){
            supplierDao.saveSupplier(supplier);
        }else {
            supplierDao.updateSupplier(supplier);
        }
    }



    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0?1:page;
        int start = (page-1)*rows;
        List<Supplier> supplierList = supplierDao.getSupplierList(start,rows,supplierName);
        map.put("total",supplierList.size());
        map.put("rows",supplierList);
        log.info("分页模糊查询供应商列表");
        return map;
    }


}
