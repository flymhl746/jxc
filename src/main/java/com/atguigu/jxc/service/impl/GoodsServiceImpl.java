package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
@Slf4j
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsDao goodsDao;
    @Autowired
    SaleListGoodsService saleListGoodsService;
    @Autowired
    CustomerReturnListGoodsService customerReturnListGoodsService;

    @Override
    public List<Goods> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        page = page == 0 ? 1:page;
       int start = (page-1) * rows;
        List<Goods>goodsList = goodsDao.listInventory(start,rows,codeOrName,goodsTypeId);

        for (Goods goods : goodsList) {

            goods.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(goods.getGoodsId())-
                    customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goods.getGoodsId()) );

        };
        log.info("首页分页查询库存listInventory..方法执行返回goodsList:");
        return goodsList;
    }

    @Override
    public List<Goods> getGoodsListByNameType(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        page = page == 0 ? 1:page;
        int start = (page-1) * rows;
        List<Goods>goodsList = goodsDao.getGoodsListByNameType(start,rows,goodsName,goodsTypeId);

        log.info("分页查询按名字类型..方法执行返回goodsList:");
        return goodsList;
    }

    @Override
    public void saveOrUpdateGoods( Goods goods) {
        if(goods.getGoodsId()==null){
            goods.setState(0);
            goods.setInventoryQuantity(0);
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            goodsDao.saveGoods(goods);
        }else {
            goodsDao.updateGoods(goods);
        }
    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() != 0) {
            return new ServiceVO(101, "有入库、有进货和销售单据的商品不能删除", null);
        } else {
            goodsDao.deleteGoods(goodsId);
            return new ServiceVO(100, "请求成功", null);
        }
    }

    @Override
    public List<Goods> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1:page;
        int start = (page-1) * rows;
        List<Goods>goodsList = goodsDao.getNoInventoryQuantity(start,rows,nameOrCode);
        return goodsList;
    }

    @Override
    public List<Goods> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page == 0 ? 1:page;
        int start = (page-1) * rows;
        List<Goods>goodsList = goodsDao.getHasInventoryQuantity(start,rows,nameOrCode);
        return goodsList;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() != 0) {
            return new ServiceVO(101, "有入库、有进货和销售单据的商品不能删除", null);
        } else {
            goodsDao.deleteStock(goodsId);
            return new ServiceVO(100, "请求成功", null);
        }

    }

    @Override
    public List<Goods> getAlarmList() {

        List<Goods> goodsList = goodsDao.getAlarmList();
        return goodsList;
    }

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();


        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }




}
