package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerDao customerDao;

    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0?1:page;
        int start = (page-1)*rows;
        List<Customer> customerList = customerDao.getCustomerList(start,rows,customerName);
        map.put("total",customerList.size());
        map.put("rows",customerList);
        log.info("分页模糊查询客户列表");
        return map;
    }

    @Override
    public void saveOrUpdateCustomer( Customer customer) {
        if(customer.getCustomerId()==null){
            customerDao.saveCustomer(customer);
        }else {
            customerDao.updateCustomer(customer);
        }
    }

    @Override
    public void deleteCustomer(String ids) {
        String[] split = ids.split(",");
        List<String> idList = Arrays.asList(split);
        List<Integer> list = idList.stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());
        customerDao.deleteCustomer(list);
    }
}
