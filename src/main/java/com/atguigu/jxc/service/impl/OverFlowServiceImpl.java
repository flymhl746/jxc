package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.*;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.OverFlowService;
import com.atguigu.jxc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OverFlowServiceImpl implements OverFlowService {
    @Autowired
    OverFlowListDao overFlowListDao;
    @Autowired
    OverFlowListGoodsDao overFlowListGoodsDao;
    @Autowired
    UserService userService;
    @Autowired
    UserDao userDao;

    @Override
    @Transactional
    public void saveOverFlowListGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {
        List<OverflowListGoods> overflowListGoodsList = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);

        log.info("damageList:{}",overflowList);
        log.info("damageList:{}",overflowListGoodsList);
        Map<String, Object> map = userService.loadUserInfo(session);
        log.info("map:{}",map);
        String userName = (String) map.get("userName");
        Integer userId = userDao.getuserId(userName);
        overflowList.setUserId(userId);

        overFlowListDao.saveOverFlowList(overflowList);
        Integer overFlowListId = overFlowListDao.getDamageListId(overflowList.getOverflowNumber());
        overflowListGoodsList = overflowListGoodsList.stream().map(damageListGoods -> {
            damageListGoods.setOverflowListId(overFlowListId);
            return damageListGoods;
        }).collect(Collectors.toList());
        overFlowListGoodsDao.saveOverFlowListGoods(overflowListGoodsList);
    }

    @Override
    public List<OverflowList> getOverFlowLists(String sTime, String eTime, HttpSession session) {
        Map<String, Object> map = userService.loadUserInfo(session);
        log.info("map:{}",map);
        String userName = (String) map.get("userName");

        List<OverflowList> overFlowLists= overFlowListDao.getOverFlowLists(sTime,eTime);
        overFlowLists =overFlowLists.stream().map(damageList -> {damageList.setTrueName(userName);
            return damageList;}).collect(Collectors.toList());
        return overFlowLists;
    }

    @Override
    public List<OverflowListGoods> getOverFlowListGoodsList(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overFlowListGoodsDao.getOverFlowListGoodsList(overflowListId);
        return overflowListGoodsList;
    }
}
