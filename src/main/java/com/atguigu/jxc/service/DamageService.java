package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface DamageService {

    void saveDamageListGoods(DamageList damageList, String damageListGoodsStr, HttpSession session);

    List<DamageList> getDamageList(String sTime, String eTime, HttpSession session);

    List<DamageListGoods> getDamageListGoodsList(Integer damageListId);
}
