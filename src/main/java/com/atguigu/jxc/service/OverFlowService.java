package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface OverFlowService {

    void saveOverFlowListGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session);

    List<OverflowList> getOverFlowLists(String sTime, String eTime, HttpSession session);

    List<OverflowListGoods> getOverFlowListGoodsList(Integer overflowListId);
}
