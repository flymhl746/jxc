package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    SupplierService supplierService;

    @PostMapping("/list")
    public Map<String,Object> getSupplierList(@RequestParam("page")Integer page,@RequestParam("rows")Integer rows,
                                              @RequestParam(value = "supplierName",required = false)String supplierName){

        Map<String,Object>map = supplierService.getSupplierList(page,rows,supplierName);
        return map;
    }
    @PostMapping("/save")
    public ServiceVO saveOrUpdateSupplier( Supplier supplier){

         supplierService.saveOrUpdateSupplier(supplier);
        return new ServiceVO(100,"请求成功",null);
    }
    // /delete
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids){

        supplierService.deleteSupplier(ids);
        return new ServiceVO(100,"请求成功",null);
    }
}
