package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @PostMapping("/list")
    public Map<String,Object> getCustomerList(@RequestParam("page")Integer page, @RequestParam("rows")Integer rows,
                                              @RequestParam(value = "customerName",required = false)String customerName){

        Map<String,Object>map = customerService.getCustomerList(page,rows,customerName);
        return map;
    }
    @PostMapping("/save")
    public ServiceVO saveOrUpdateCustomer( Customer customer){

        customerService.saveOrUpdateCustomer(customer);
        return new ServiceVO(100,"请求成功",null);
    }
    // /delete
    @PostMapping("/delete")
    public ServiceVO deleteCustomer(String ids){

        customerService.deleteCustomer(ids);
        return new ServiceVO(100,"请求成功",null);
    }
}
