package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageController {
    @Autowired
    DamageService damageService;

    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(DamageList damageList, String damageListGoodsStr, HttpSession session){

        damageService.saveDamageListGoods(damageList,damageListGoodsStr,session);
        return new ServiceVO(100,"请求成功",null);
    }
    @PostMapping("/list")
    public Map<String,Object> getDamageList(String  sTime, String  eTime,HttpSession session){
        List<DamageList> damageLists = damageService.getDamageList(sTime,eTime, session);
        Map<String,Object>goodsMap = new HashMap<>();
        goodsMap.put("rows",damageLists);
        return goodsMap;
    }
    @PostMapping("/goodsList")
    public Map<String,Object> getDamageListGoodsList(Integer damageListId){
        List<DamageListGoods> damageListGoodsList = damageService.getDamageListGoodsList(damageListId);
        Map<String,Object>goodsMap = new HashMap<>();
        goodsMap.put("rows",damageListGoodsList);
        return goodsMap;
    }
}
