package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverFlowController {
    @Autowired
    OverFlowService overFlowService;
    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(OverflowList overflowList, String overflowListGoodsStr, HttpSession session){

        overFlowService.saveOverFlowListGoods(overflowList,overflowListGoodsStr,session);
        return new ServiceVO(100,"请求成功",null);
    }
    @PostMapping("/list")
    public Map<String,Object> getOverFlowLists(String  sTime, String  eTime, HttpSession session){
        List<OverflowList> overFlowLists = overFlowService.getOverFlowLists(sTime,eTime, session);
        Map<String,Object>goodsMap = new HashMap<>();
        goodsMap.put("rows",overFlowLists);
        return goodsMap;
    }
    @PostMapping("/goodsList")
    public Map<String,Object> getOverFlowListGoodsList(Integer overflowListId){
        List<OverflowListGoods> overflowListGoodsList = overFlowService.getOverFlowListGoodsList(overflowListId);
        Map<String,Object>goodsMap = new HashMap<>();
        goodsMap.put("rows",overflowListGoodsList);
        return goodsMap;
    }
}
