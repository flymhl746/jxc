package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.OverflowList;

import java.util.List;

public interface OverFlowListDao {

    void saveOverFlowList(OverflowList overflowList);

    Integer getDamageListId(String overflowNumber);

    List<OverflowList> getOverFlowLists(String sTime, String eTime);
}
