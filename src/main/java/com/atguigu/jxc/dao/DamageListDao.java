package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {
    void saveDamageList(DamageList damageList);

    Integer getDamageListId(String damageNumber);

    List<DamageList> getDamageListByTime(@Param("sTime") String sTime, @Param("eTime")String eTime);
}
