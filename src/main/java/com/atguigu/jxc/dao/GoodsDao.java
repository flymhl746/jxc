package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */

public interface GoodsDao {


    String getMaxCode();


    List<Goods> listInventory(@Param("start")int start, @Param("rows") Integer rows, @Param("codeOrName")String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsListByNameType(@Param("start")int start, @Param("rows")Integer rows, @Param("goodsName")String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    void saveGoods(Goods goods);

    void updateGoods( Goods goods);

    Goods getGoodsById(Integer goodsId);

    void deleteGoods(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("start") int start, @Param("rows")Integer rows, @Param("nameOrCode")String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("start") int start, @Param("rows")Integer rows, @Param("nameOrCode")String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity,@Param("purchasingPrice")  double purchasingPrice);

    void deleteStock(Integer goodsId);

    List<Goods> getAlarmList();

    List<Goods> getGoodsListByGoodsTypeId(Integer goodsTypeId);
}
