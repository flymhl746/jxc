package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import java.util.List;
/**
 * @description
 */

public interface SupplierDao {


    List<Supplier> getSupplierList(@Param("start") int start, @Param("rows")Integer rows,@Param("supplierName") String supplierName);


    void saveSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void deleteSupplier(@Param("idList") List<Integer> idList);
}
