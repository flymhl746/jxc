package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverFlowListGoodsDao {

     void saveOverFlowListGoods(List<OverflowListGoods> overflowListGoodsList);

    List<OverflowListGoods> getOverFlowListGoodsList(Integer overflowListId);
}
