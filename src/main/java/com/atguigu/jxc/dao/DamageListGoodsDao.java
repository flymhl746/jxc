package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageListGoodsDao {

    void saveDamageListGoods(List<DamageListGoods> damageListGoodsList);

    List<DamageListGoods> getDamageListGoodsList(Integer damageListId);
}
