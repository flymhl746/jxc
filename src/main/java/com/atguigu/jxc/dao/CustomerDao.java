package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description
 */

public interface CustomerDao {


    List<Customer> getCustomerList(@Param("start") int start, @Param("rows")Integer rows, @Param("customerName") String customerName);

    void saveCustomer(Customer customer);

    void updateCustomer( Customer customer);

    void deleteCustomer(@Param("idList") List<Integer> list);
}
